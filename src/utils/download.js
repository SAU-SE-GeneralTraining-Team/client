function downloadFile(blob, fileName) {
  const url = window.URL.createObjectURL(blob)
  const link = document.createElement('a')
  link.style.display = 'none'
  link.download = fileName
  link.href = url
  link.click()
  URL.revokeObjectURL(url)
}

export function downloadExcel(res) {
  const disposition = res.headers['content-disposition']
  const fileName = disposition.substring(disposition.indexOf('=') + 1)
  const blob = new Blob([res.data], { type: 'application/vnd.ms-excel;charset=UTF-8' })
  downloadFile(blob, fileName)
}

export function downloadAttachs(res) {
  const disposition = res.headers['content-disposition']
  // 文件名
  const fileName = disposition.substring(disposition.indexOf('=') + 1)
  // 文件后缀名
  const suffix = disposition.substring(disposition.indexOf('.') + 1)
  if (suffix === 'xls') {
    const blob = new Blob([res.data], { type: 'application/vnd.ms-excel;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'zip') {
    const blob = new Blob([res.data], { type: 'application/application/zip;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'doc') {
    const blob = new Blob([res.data], { type: 'application/msword;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'pdf') {
    const blob = new Blob([res.data], { type: 'application/pdf;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'xlsx') {
    const blob = new Blob([res.data], { type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'png') {
    const blob = new Blob([res.data], { type: 'image/png;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'jpg' || suffix === 'jpeg') {
    const blob = new Blob([res.data], { type: 'image/jpeg;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
  if (suffix === 'txt') {
    const blob = new Blob([res.data], { type: 'text/plain;charset=UTF-8' })
    downloadFile(blob, fileName)
  }
}
