import request from '@/utils/request'

// 发起讨论
export function createIssue(data) {
  return request({
    url: '/sys/createIssue',
    method: 'post',
    data
  })
}

// 获取问题列表
export function getIssueList(listQuery) {
  return request({
    url: '/sys/getIssueList',
    method: 'get',
    params: listQuery
  })
}

// 创建一级回复
export function createFirstComment(data) {
  return request({
    url: '/sys/createFirstComment',
    method: 'post',
    data
  })
}

// 创建二级回复
export function createSecondComment(data) {
  return request({
    url: '/sys/createSecondComment',
    method: 'post',
    data
  })
}

// 获取评论信息
export function getComment(postid) {
  return request({
    url: '/sys/getComment',
    method: 'get',
    params: { postid }
  })
}
