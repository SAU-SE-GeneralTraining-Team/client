import request from '@/utils/request'

export function addStandard(data) {
  return request({
    url: '/sys/addStandard',
    method: 'post',
    data
  })
}

export function getStandardList(listQuery) {
  return request({
    url: '/sys/getStandardList',
    method: 'get',
    params: listQuery
  })
}

// 获取行业标准正文内容
// export function getMaintext(number) {
//   return request({
//     url: `/sys/getMaintext?number=${number}`,
//     method: 'get'
//   })
// }

export function searchDetailInfo(number) {
  return request({
    url: `/sys/searchStandardDetail?number=${number}`,
    method: 'get'
  })
}

// 下载行业标准附件
export function downloadAttach(path) {
  return request({
    url: '/sys/downloadStandardAttach',
    method: 'post',
    data: { path },
    responseType: 'blob'
  })
}

export function updateStandardInfo(data) {
  return request({
    url: '/sys/updateStandardInfo',
    method: 'post',
    data
  })
}

export function deleteStandard(number) {
  return request({
    url: '/sys/deleteStandard',
    method: 'post',
    data: { number }
  })
}
