import request from '@/utils/request'

export function addCase(data) {
  return request({
    url: '/sys/addCase',
    method: 'post',
    data
  })
}

export function queryCase(listQuery) {
  return request({
    url: `/sys/getCaseList`,
    method: 'get',
    params: listQuery
  })
}
export function downloadCaseFile(path) {
  return request({
    url: '/sys/downloadCaseFile',
    method: 'post',
    data: { path },
    responseType: 'blob'
  })
}

export function getCaseInfoById(cid) {
  return request({
    url: '/sys/getCaseInfoById',
    method: 'get',
    params: { cid }
  })
}

export function updateCaseInfo(data) {
  return request({
    url: '/sys/updateCaseInfo',
    method: 'post',
    data
  })
}

export function deleteCase(cid) {
  return request({
    url: '/sys/deleteCase',
    method: 'post',
    data: { cid }
  })
}
