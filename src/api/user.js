import request from '@/utils/request'

export function login(data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

export function getInfo(token) {
  return request({
    url: '/sys/getInfo',
    method: 'get',
    params: { token }
  })
}

export function logout(token) {
  return request({
    url: '/sys/logout',
    method: 'get',
    params: { token }
  })
}

export function getUserList(listQuery) {
  return request({
    url: '/sys/getUserList',
    method: 'get',
    params: listQuery
  })
}

export function addUser(data) {
  return request({
    url: '/sys/addUser',
    method: 'post',
    data
  })
}

export function getUserListByRole(listQuery, role) {
  return request({
    url: `/sys/getUserListByRole?role=${role}&page=${listQuery.page}&limit=${listQuery.limit}`,
    method: 'get'
  })
}

export function updateUserInfo(data) {
  return request({
    url: '/sys/updateUserInfo',
    method: 'post',
    data
  })
}

export function resetPassword(data) {
  return request({
    url: '/sys/resetPassword',
    method: 'post',
    data
  })
}

export function downloadExportTemplate() {
  return request({
    url: '/sys/dowmloadExportTemplate',
    method: 'post',
    responseType: 'blob'
  })
}

export function modifyUserInfo(data) {
  return request({
    url: '/sys/updateUserInfo',
    method: 'post',
    data
  })
}
