import request from '@/utils/request'

export function getOperlog(listQuery) {
  return request({
    url: '/sys/getOperLogs',
    method: 'post',
    data: listQuery
  })
}
