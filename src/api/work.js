import request from '@/utils/request'

export function getWorkList(listQuery) {
  return request({
    url: '/sys/getWorklist',
    method: 'get',
    params: listQuery
  })
}
