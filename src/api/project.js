import request from '@/utils/request'

export function getProList(listQuery) {
  return request({
    url: '/sys/getProList',
    method: 'get',
    params: listQuery
  })
}

export function getProInfoById(id) {
  return request({
    url: '/sys/getProInfoById',
    method: 'get',
    params: { id }
  })
}

export function updateProInfo(data) {
  return request({
    url: '/sys/updateProInfo',
    method: 'post',
    data
  })
}

export function addParticipant(data) {
  return request({
    url: '/sys/addParticipant',
    method: 'post',
    data
  })
}

export function getParticipant(id) {
  return request({
    url: '/sys/getParticipate',
    method: 'get',
    params: { id }
  })
}

export function deleteParticipate(data) {
  return request({
    url: '/sys/deleteParticipate',
    method: 'post',
    data
  })
}

export function updateProRole(data) {
  return request({
    url: '/sys/updateProRole',
    method: 'post',
    data
  })
}

export function addProgress(data) {
  return request({
    url: '/sys/addProgress',
    method: 'post',
    data
  })
}
export function updateCommentInfo(data) {
  return request({
    url: '/sys/updateComment',
    method: 'post',
    data
  })
}

export function getDetail(projid) {
  return request({
    url: '/sys/getDetail',
    method: 'get',
    params: { projid }
  })
}

export function deletePro(projid) {
  return request({
    url: '/sys/deletePro',
    method: 'post',
    data: { projid }
  })
}

// 项目附件下载
export function downloadProFile(projid) {
  return request({
    url: '/sys/downloadProAttach',
    method: 'post',
    data: { projid },
    responseType: 'blob'
  })
}

// 项目动态附件下载
export function downloadProgressFile(path) {
  return request({
    url: '/sys/downloadProgressAttach',
    method: 'post',
    data: { path },
    responseType: 'blob'
  })
}
