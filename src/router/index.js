import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
    roles: ['admin','editor']    control the page roles (you can set multiple roles)
    title: 'title'               the name show in sidebar and breadcrumb (recommend set)
    icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
    breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
    activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
  }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/case',
    meta: { title: '项目案例', icon: 'dashboard' },
    children: [
      {
        path: 'case',
        name: 'Case',
        component: () => import('@/views/case/list'),
        meta: { title: '案例列表' },
        hidden: true
      },
      {
        path: '/case/create',
        component: () => import('@/views/case/create'),
        meta: { title: '新增案例' },
        hidden: true
      },
      {
        path: '/case/edit',
        component: () => import('@/views/case/edit'),
        meta: { title: '编辑案例' },
        hidden: true
      }
    ]
  },

  {
    path: '/standard',
    component: Layout,
    redirect: '/standard/list',
    meta: { title: '行业标准', icon: 'form' },
    children: [
      {
        path: 'list',
        component: () => import('@/views/standard/list'),
        meta: { title: '标准列表' },
        hidden: true
      },
      {
        path: 'addStandard',
        component: () => import('@/views/standard/create'),
        meta: { title: '添加标准' },
        hidden: true
      },
      {
        path: 'edit',
        component: () => import('@/views/standard/edit'),
        meta: { title: '编辑标准' },
        hidden: true
      }
    ]
  },

  {
    path: '/work',
    component: Layout,
    children: [
      {
        path: 'list',
        name: 'WorkList',
        component: () => import('@/views/work/list'),
        meta: { title: '学生作品展示', icon: 'example' }
      },
      {
        path: 'detail',
        name: 'ProjectDetail',
        component: () => import('@/views/detail'),
        meta: { title: '项目详情' },
        hidden: true
      }
    ]
  }

]

export const asyncRoutes = [
  {
    path: '/project',
    component: Layout,
    redirect: '/project/list',
    meta: { title: '我的项目', icon: 'table', roles: ['学生', '校内老师', '企业导师'] },
    children: [
      {
        path: 'list',
        component: () => import('@/views/project/list'),
        meta: { title: '项目列表' },
        hidden: true
      },
      {
        path: 'addProject',
        component: () => import('@/views/project/create'),
        meta: { title: '新增项目' },
        hidden: true
      },
      {
        path: 'edit/',
        name: 'ProEdit',
        component: () => import('@/views/project/edit'),
        meta: { title: '编辑项目' },
        hidden: true
      },
      {
        path: 'participants',
        name: 'Participants',
        component: () => import('@/views/project/participant'),
        meta: { title: '项目成员' },
        hidden: true
      }
      // {
      //   path: 'detail',
      //   name: 'ProjectDetail',
      //   component: () => import('@/views/detail'),
      //   meta: { title: '项目详情' },
      //   hidden: true
      // }
    ]
  },

  {
    path: '/issue',
    component: Layout,
    redirect: '/issue/list',
    meta: { title: '讨论区', icon: 'link', roles: ['学生', '校内老师', '企业导师'] },
    children: [
      {
        path: 'list',
        component: () => import('@/views/discussion/list'),
        meta: { title: '讨论列表' },
        hidden: true
      },
      {
        path: 'create',
        component: () => import('@/views/discussion/create'),
        meta: { title: '发起讨论' },
        hidden: true
      },
      {
        path: 'comment',
        component: () => import('@/views/discussion/components/IssueComment'),
        meta: { title: '讨论详情' },
        hidden: true
      }
    ]
  },

  {
    path: '/user',
    component: Layout,
    redirect: '/user/list',
    name: 'User',
    meta: { title: '用户管理', icon: 'user', roles: ['系统管理员'] },
    children: [
      {
        path: 'list',
        name: 'List',
        component: () => import('@/views/user/list'),
        meta: { title: '用户列表', roles: ['系统管理员'] },
        hidden: true
      },
      {
        path: 'add',
        name: 'Add',
        component: () => import('@/views/user/create'),
        meta: { title: '添加用户', roles: ['系统管理员'] },
        hidden: true
      }
    ]
  },

  {
    path: '/log',
    component: Layout,
    children: [
      {
        path: 'oper',
        component: () => import('@/views/operlog'),
        meta: { title: '操作日志', icon: 'example', roles: ['系统管理员'] }
      }
    ]
  },

  // 404 page must be placed at the end !!!
  { path: '*', redirect: '/404', hidden: true }

]

const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
